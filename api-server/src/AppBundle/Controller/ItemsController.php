<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\UploadedFile;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use AppBundle\Entity\Item;
use AppBundle\Form\ItemType;

class ItemsController extends Controller
{
    /**
     * @Route("/items")
     * @Method({"GET"})
     */
    public function listAction()
    {
      $repository = $this->getDoctrine()->getRepository('AppBundle:Item');
      $items = $repository->findAll();

      $jsonContent = json_encode($items);

      $response = new Response($jsonContent);
      $response->headers->set('Content-Type', 'application/json');
      return $response;

    }

    /**
     * @Route("/items")
     * @Method({"POST"})
     */
    public function createAction(Request $request)
    {
      $repository = $this->getDoctrine()->getRepository('AppBundle:Item');
      $item = new Item();

      $form = $this->createForm(new ItemType(), $item);
      $form->bind(json_decode($request->getContent(),true));

      $item->setCreatedDate(new \DateTime("now"));
      $item->setUpdatedDate(new \DateTime("now"));

      // Validate data
      $validator = $this->get('validator');
      $errors = $validator->validate($item);

      // If data is invalid
      if (count($errors) > 0)
      {
        $responseData = array();
        foreach ($errors as $error) {
          $responseData[] = $error->getMessage();
        }
        $response = new Response(json_encode($responseData), 422);
        return $response;
      }

      // Else update item
      $em = $this->getDoctrine()->getEntityManager();
      $em->persist($item);
      $em->flush();

      $response = new Response(json_encode($item));

      return $response;
    }

    /**
     * @Route("/items/{id}")
     * @Method({"PUT"})
     */
    public function updateAction(Request $request, $id)
    {
      $repository = $this->getDoctrine()->getRepository('AppBundle:Item');
      $item = $repository->find($id);

      // If item isn't exists
      if (!$item instanceof Item) {
        $response = new Response("Item not found", 404);
        return $response;
      }

      $form = $this->createForm(new ItemType(), $item);
      $form->bind(json_decode($request->getContent(),true));

      // Validate data
      $validator = $this->get('validator');
      $errors = $validator->validate($item);

      // If data is invalid
      if (count($errors) > 0)
      {
        $responseData = array();
        foreach ($errors as $error) {
          $responseData[] = $error->getMessage();
        }
        $response = new Response(json_encode($responseData), 422);
        return $response;
      }

      // Else update item
      $item->setUpdatedDate(new \DateTime("now"));

      $em = $this->getDoctrine()->getEntityManager();
      $em->flush();

      $response = new Response(json_encode($item));

      return $response;
    }

    /**
     * @Route("/items/{id}")
     * @Method({"DELETE"})
     */
    public function deleteAction($id)
    {
      $item = $this->getDoctrine()->getRepository('AppBundle:Item')->find($id);

      if (!$item instanceof Item) {
        throw new NotFoundHttpException('Item not found');
      }

      $em = $this->getDoctrine()->getEntityManager();
      $em->remove($item);
      $em->flush();

      $response = new Response("");
      return $response;
    }

    /**
     * @Route("/items/upload_photo")
     * @Method({"POST"})
     */
    public function uploadPhotoAction(Request $request)
    {
      $uploaded_file = $request->files->get('file');

      $name = uniqid().'.'.$uploaded_file->guessExtension();

      if (!is_dir($this->getUploadDir()))
      {
        mkdir($this->getUploadDir());
      }

      $file = $uploaded_file->move($this->getUploadDir(), $name);

      $response = new Response($this->getUri().'/'.$name);
      return $response;
    }

    protected function getUploadDir()
    {
      return './images';
    }

    protected function getUri()
    {
      $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ||
        $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
      $domainName = "some-api.zz.mu";
      return $protocol.$domainName.'/images';
    }

    protected function mapErrorArray($error)
    {
      return $error->getMessage();
    }
}
