<?php
	namespace AppBundle\Entity;

	use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;

    use JsonSerializable;

	/**
	* @ORM\Entity
	* @ORM\Table(name="item")
	*/
	class Item implements JsonSerializable
    {
		/**
		* @ORM\Id
		* @ORM\Column(type="integer")
		* @ORM\GeneratedValue(strategy="AUTO")
		*/
		protected $id;

		/**
		* @ORM\Column(type="string", length=255)
        * @Assert\NotBlank(message="Title should not be blank")
		*/
		protected $title;

		/**
		* @ORM\Column(type="text", nullable=true)
		*/
		protected $description = null;

		/**
		* @ORM\Column(type="text", nullable=true)
		*/
		protected $img_url = null;

		/**
		* @ORM\Column(type="datetime")
		*/
		protected $created_date;

		/**
		* @ORM\Column(type="datetime")
		*/
		protected $updated_date;

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set title
         *
         * @param string $title
         * @return Item
         */
        public function setTitle($title)
        {
            $this->title = $title;

            return $this;
        }

        /**
         * Get title
         *
         * @return string
         */
        public function getTitle()
        {
            return $this->title;
        }

        /**
         * Set description
         *
         * @param string $description
         * @return Item
         */
        public function setDescription($description)
        {
            $this->description = $description;

            return $this;
        }

        /**
         * Get description
         *
         * @return string
         */
        public function getDescription()
        {
            return $this->description;
        }

        /**
         * Set img_url
         *
         * @param string $imgUrl
         * @return Item
         */
        public function setImgUrl($imgUrl)
        {
            $this->img_url = $imgUrl;

            return $this;
        }

        /**
         * Get img_url
         *
         * @return string
         */
        public function getImgUrl()
        {
            return $this->img_url;
        }

        /**
         * Set created_date
         *
         * @param \DateTime $createdDate
         * @return Item
         */
        public function setCreatedDate($createdDate)
        {
            $this->created_date = $createdDate;

            return $this;
        }

        /**
         * Get created_date
         *
         * @return \DateTime
         */
        public function getCreatedDate()
        {
            return $this->created_date;
        }

        /**
         * Set updated_date
         *
         * @param \DateTime $updatedDate
         * @return Item
         */
        public function setUpdatedDate($updatedDate)
        {
            $this->updated_date = $updatedDate;

            return $this;
        }

        /**
         * Get updated_date
         *
         * @return \DateTime
         */
        public function getUpdatedDate()
        {
            return $this->updated_date;
        }

        public function jsonSerialize()
        {
            return array(
                'id' => $this->getId(),
                'title' => $this->getTitle(),
                'description' => $this->getDescription(),
                'img_url' => $this->getImgUrl(),
                'created_date' => $this->getCreatedDate()->format('D M d Y H:i:s O'),
                'updated_date' => $this->getUpdatedDate()->format('D M d Y H:i:s O')
            );
        }
}
