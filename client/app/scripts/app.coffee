'use strict'

###*
 # @ngdoc overview
 # @name clientApp
 # @description
 # # clientApp
 #
 # Main module of the application.
###
angular
  .module 'clientApp', [
    'ngResource'
    'ngFileUpload'
    'ui.bootstrap'
  ]
  .constant 'API_SERVER','http://some-api.zz.mu'
