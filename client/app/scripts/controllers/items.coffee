'use strict'

###*
 # @ngdoc function
 # @name clientApp.controller:ItemsCtrl
 # @description
 # # MainCtrl
 # Controller of the clientApp
###
angular.module 'clientApp'
  .controller 'ItemsCtrl', ($scope, $timeout, $modal, Item, Upload, API_SERVER) ->
    $scope.server = API_SERVER

    update = ->
      Item.query (items) ->
        $scope.items = items

    update()

    openModal = (size) ->
      modalInstance = $modal.open
        animation: $scope.animationsEnabled
        controller: 'ModalCtrl'
        templateUrl: 'views/create_model.html'
        resolve:
          current_item: ->
            $scope.current_item

    $scope.isUndefined = (thing) ->
      thing?

    $scope.create = ->
      $scope.current_item = new Item {}
      modal = openModal()
      modal.result.then ->
        update()
      .catch (reason) ->
        update()
        console.log 'Dismiss', reason

    $scope.edit = (item) ->
      $scope.current_item = jQuery.extend {}, item
      modal = openModal()
      modal.result.then ->
        update()
      .catch (reason) ->
        update()
        console.log 'Dismiss', reason

    $scope.delete = (item) ->
      if confirm 'Are you sure you want to delete this product?'
        item.$remove().then update
        .catch (res) ->
          switch res.status
            when 404
              alert('Can\'t find item. May be it already removed.')
              update()


