angular.module 'clientApp'
  .directive 'fallbackSrc', ->
    link: (scope, element, attrs) ->
      element.bind 'error', ->
        angular.element(this).attr 'src', attrs.fallbackSrc

  .directive 'actualSrc', ->
    link: (scope, element, attrs) ->
      attrs.$observe 'actualSrc', (newVal, oldVal) ->
        console.log oldVal
        unless oldVal?
          img = new Image
          img.src = attrs.actualSrc
          angular.element(img).bind 'load', ->
            element.attr 'src', attrs.actualSrc
