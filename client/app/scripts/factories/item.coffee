angular.module 'clientApp'
  .factory 'Item', ($resource, $http, API_SERVER) ->
    resource = $resource API_SERVER + '/items/:id'
    ,
      id: '@id'
    ,
      update:
        method: 'put'
        isArray: false
      create: method: 'post'


    resource::$save = ->
      if !@id
        @$create()
      else
        @$update()

    resource
